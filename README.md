# tech-hunter-app
```
upload the 'dist' folder content to FTP (mytechhunder.dev)
```
## Project setup
```
npm install or npm i
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
